package com.pawelbanasik.main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.pawelbanasik.config.DemoConfig;
import com.pawelbanasik.dao.AccountDao;
import com.pawelbanasik.dao.MembershipDao;
import com.pawelbanasik.domain.Account;

public class MainDemoApp {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		AccountDao accountDao = context.getBean("accountDao", AccountDao.class);
		MembershipDao membershipDao = context.getBean("membershipDao", MembershipDao.class);
		Account account = new Account();
		accountDao.addAccount(account, true);
		accountDao.doWork();
		membershipDao.addSillyMember();
		membershipDao.goToSleep();
		context.close();

	}

}
