package com.pawelbanasik.dao;

import org.springframework.stereotype.Component;

import com.pawelbanasik.domain.Account;

@Component
public class AccountDao {

	public void addAccount(Account account, boolean vipFlag) {
		System.out.println(getClass().getSimpleName() + ": Doing my db work: Adding and account.");
	}

	public boolean doWork() {
		System.out.println(getClass().getSimpleName() + ": doWork()");
		return false;
	}
	
}
